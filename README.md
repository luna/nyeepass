# nyeepass

CLI to .kdbx **(only)** database files

## System depedencies

 - The only required system depedencies are for clipboard support,
    which are declared [here](https://pypi.org/project/pyperclip)
## Installing

```
git clone https://gitlab.com/lnmds/nyeepass.git
cd nyeepass
pip install --editable .
```

## Configuring & Running

Create a configuration file at `~/.nyeepass.yml`

**KEEP IN MIND** the order of the given `credentials` matters.
You must put **`password`** first.

Yes, you can have more than one password. Dunno how other keepass
programs handle that kind of behavior, but we do!

```yaml
databases:
  dab:
    path: ~/test_database/test.kdbx
  dab2:
    path: ~/test_database/test_keyfile.kdbx
    keyfile: ~/ass.key
    credentials: password keyfile

default_db: dab
```

Launch `nyeepass open` or `nyeepass open any_other_database`,
Insert password as needed, declare keyfile if any, you will
be dropped into nyeesh if credentials are correct, send `?`
to find the commands.

TAB-completion works on nyeesh, including completion of your entries,
so `copypwd g<TAB>` will prompt all entries starting with `g`.
