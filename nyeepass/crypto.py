import hashlib


def sha256(data: bytes) -> bytes:
    if isinstance(data, str):
        data = data.encode("utf-8")

    hash_data = hashlib.sha256(data)
    return hash_data.digest()
