# cython: language_level=3

import time
import click
from Crypto.Cipher import AES
from .consts import HeaderField

cdef str transform_key(dict dyn_header, str composite_key):
    # step 2: generate the final master key
    # the final master key needs stuff from the dyn_header
    transform_seed = dyn_header[HeaderField.TRANSFORM_SEED]
    transform_rounds = dyn_header[HeaderField.TRANSFORM_ROUNDS]

    cipher = AES.new(transform_seed, AES.MODE_ECB)

    transformed_key = composite_key

    # this is the main anti-bruteforce of KeePass,
    # as you can put a high transform_rounds value
    # to make it difficult for an attacker to guess
    click.secho(f'Key rounds uwu: {transform_rounds}', fg='yellow')

    t_start = time.monotonic()
    for _ in range(transform_rounds):
        transformed_key = cipher.encrypt(transformed_key)

    t_end = time.monotonic()
    seconds = round(t_end - t_start, 5)
    click.secho(f'Took {seconds}sec to generate transformed key',
                fg='yellow')

    return transformed_key
