import struct
import gzip
import io
import base64
import uuid
import time
from pathlib import Path
from functools import reduce

import click
from bs4 import BeautifulSoup
from lxml import etree
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

from Crypto.Cipher import AES
from salsa20 import Salsa20_keystream

from .consts import HeaderField
from .crypto import sha256

# lots of constants for database
KEEPASS_PRIMARY_ID = b"\x03\xd9\xa2\x9a"
MAIN_CIPHER_ID = "31c1f2e6bf714350be585216afc5aff"


def _uuid(string):
    return uuid.UUID(bytes=base64.b64decode(string.encode()))


def _to_hex(data: str):
    """Print string data in hex encoding"""
    # res = ''.join([hex(ord(c) if isinstance(c, str) else c)[2:] for c in data])
    lst = []
    for ch in data:
        hv = hex(ord(ch) if isinstance(ch, str) else ch).replace("0x", "")
        if len(hv) == 1:
            hv = "0" + hv
        lst.append(hv)

    res = reduce(lambda x, y: x + y, lst)
    return f"{res} {len(data)}"


def _xor(aa, bb):
    """Return a bytearray of a bytewise XOR of `aa` and `bb`."""
    return bytes([a ^ b for a, b in zip(bytes(aa), bytearray(bb))])


class DatabaseError(Exception):
    pass


class Database:
    def __init__(self, config):
        self.config = config
        self.raw_path = self.config["path"]
        self.closed = False

        self.attrs = {}
        self.dyn_header = None

        self._decrypted_data = None
        self._cipher_mappings = {}
        self.data = {}

    def __repr__(self):
        ext_str = " ".join([f"{key}:{self.attrs[key]}" for key in self.attrs])
        return f"<Database path={self.path} {ext_str}>"

    def _raw_to_field_data(self, field: HeaderField, field_size: int, raw_data: bytes):
        """Convert from raw bytes information to the proper
        type required for a field."""
        varstr = f"{field_size}s"
        fmt = {
            HeaderField.COMMENT: varstr,
            HeaderField.CIPHERID: varstr,
            HeaderField.COMPRESSION_FLAGS: "<i",
            HeaderField.MASTER_SEED: varstr,
            HeaderField.TRANSFORM_SEED: varstr,
            HeaderField.TRANSFORM_ROUNDS: "<q",
            HeaderField.ENCRYPTION_IV: varstr,
            HeaderField.PROTECTEDSTREAMKEY: varstr,
            HeaderField.STREAMSTARTBYTES: varstr,
            HeaderField.INNERRANDOMSTREAMID: "<i",
        }.get(field)

        if fmt is None:
            return None

        if fmt == varstr:
            return raw_data
        else:
            return struct.unpack(fmt, raw_data)[0]

    def _check_field_sane(self, field, field_size, field_data):
        func = {
            # HeaderField.CIPHERID: lambda data: _to_hex(field_data) == MAIN_CIPHER_ID,
            HeaderField.COMPRESSION_FLAGS: lambda data: data in (0, 1),
            HeaderField.MASTER_SEED: lambda data: len(data) == field_size,
            HeaderField.INNERRANDOMSTREAMID: lambda data: data in (0, 1, 2),
        }.get(field)

        if func is None:
            return

        valid = func(field_data)

        if not valid:
            raise Exception(f"Error validating field {field}: {field_data!r}")

    def _read_dyn_header(self, fd):
        dyn_header = {}

        while True:
            # one byte for field ID
            field_id_bytes = fd.read(1)
            assert len(field_id_bytes) == 1
            (field_id,) = struct.unpack("B", field_id_bytes)

            field = HeaderField(field_id)

            print("field is", field)

            if field in dyn_header:
                print("exist")
                continue

            # two bytes for field size
            field_size = fd.read(2)
            (field_size,) = struct.unpack("<h", field_size)

            # field_id 0 means stop
            # yes, we read the field_size as described in the specs
            if field_id == 0:
                print("end dyn header")
                return dyn_header

            raw_data = fd.read(field_size)

            # convert raw bytes information into the wanted field (e.g an int)
            field_data = self._raw_to_field_data(field, field_size, raw_data)

            if field_data is None:
                print("no field data")
                continue

            # check is the field is valid (matches with our constants,
            # has good length, etc)
            self._check_field_sane(field, field_size, field_data)

            # put it in the header
            if isinstance(field_data, bytes):
                print(f"dyn header, fieldId {field_id} fieldData {_to_hex(field_data)}")
            else:
                print(
                    f"dyn header, fieldId {field_id} ({type(field_data)}) {field_data}"
                )

            dyn_header[field] = field_data

    def fill(self):
        self.path = Path(self.raw_path).expanduser()
        fd = self.path.open(mode="rb")

        # check first signature of the file
        sig1 = fd.read(4)

        if sig1 != KEEPASS_PRIMARY_ID:
            raise DatabaseError("Signature 1 failure")

        # TODO: validate sig2 data
        # sig2 = fd.read(4)
        fd.read(4)
        # res = map(hex, list(sig2))

        # read keepass db file version
        version = fd.read(4)
        (minor_version, major_version) = struct.unpack("<hh", version)
        self.attrs["version"] = (major_version, minor_version)

        print(self.attrs["version"])

        dyn_header = self._read_dyn_header(fd)

        # dynamic header is fetched and succesfully validated
        self.dyn_header = dyn_header
        self._fd = fd
        self._payload_start = fd.tell() + 4

    def _unpad(self, data: bytes) -> bytes:
        print(len(data), bytearray(data)[-1])
        return data[: len(data) - bytearray(data)[-1]]

    def _transform_key(self, composite_key):
        # step 2: generate the final master key
        # the final master key needs stuff from the dyn_header
        transform_seed = self.dyn_header[HeaderField.TRANSFORM_SEED]
        transform_rounds = self.dyn_header[HeaderField.TRANSFORM_ROUNDS]
        print(f"transform seed: {_to_hex(transform_seed)}, rounds: {transform_rounds}")

        cipher = AES.new(transform_seed, AES.MODE_ECB)

        transformed_key = composite_key

        # this is the main anti-bruteforce of KeePass,
        # as you can put a high transform_rounds value
        # to make it difficult for an attacker to guess
        click.secho(f"Key rounds: {transform_rounds}", fg="yellow")

        t_start = time.monotonic()
        for idx in range(transform_rounds):
            aes_input = transformed_key
            transformed_key = cipher.encrypt(aes_input)

            if 0 <= idx <= 2:
                print(
                    f"round {idx}: in {_to_hex(aes_input)} out {_to_hex(transformed_key)}"
                )

        t_end = time.monotonic()
        seconds = round(t_end - t_start, 5)
        click.secho(f"Took {seconds}sec to generate transformed key", fg="yellow")

        return transformed_key

    def decrypt(self, components):
        """Open and decrypt the database."""
        # step 1: generate the composite key
        # based on the components of it (keyfile(s) and password(s))

        # concatenate all of the components, hash it all,
        # and we get our composite key
        print("components: ", [_to_hex(x) for x in components])
        composite_key = sha256(b"".join(components))
        print(f"composite key {_to_hex(composite_key)}")

        # step 2: transformed key
        transformed_key = self._transform_key(composite_key)
        transformed_key = sha256(transformed_key)
        print(f"transformed key {_to_hex(transformed_key)}")

        # step 3: acquire master key
        master_seed = self.dyn_header[HeaderField.MASTER_SEED]
        print(f"master seed {_to_hex(master_seed)}")

        master_key = sha256(master_seed + transformed_key)
        print(f"master_key {_to_hex(master_key)}")

        # since we got the final master key, we must construct another
        # AES context with that key and the IV (this IV is given on the
        # database file)

        encryption_iv = self.dyn_header[HeaderField.ENCRYPTION_IV]
        cipher = Cipher(
            algorithms.AES(master_key),
            modes.CBC(encryption_iv),
            backend=default_backend(),
        )

        decryptor = cipher.decryptor()

        # now, with the decryptor, we can check if our key is the correct one
        # by reading len(STREAMSTARTBYTES) from payload start and checking
        # with what is in the headers.

        stream_start = self.dyn_header[HeaderField.STREAMSTARTBYTES]
        stream_len = len(stream_start)

        self._fd.seek(self._payload_start)

        # payload area is composed of STREAMSTARTBYTES + <everything we want>
        payload = self._fd.read()

        decrypted_payload = decryptor.update(payload) + decryptor.finalize()
        decrypted_payload = self._unpad(decrypted_payload)

        file_start_stream = decrypted_payload[:stream_len]

        if stream_start != file_start_stream:
            raise DatabaseError("Failed to decrypt database")

        # the key is correct! we did it reddit!
        payload = decrypted_payload[stream_len:]

        # we can get the XML describing the database after reading its blocks.
        stream = io.BytesIO(payload)
        blocks = {}

        while True:
            (block_id,) = struct.unpack("<i", stream.read(4))
            block_hash = stream.read(32)

            (block_size,) = struct.unpack("<i", stream.read(4))
            print(
                "block", block_id, ", hash", _to_hex(block_hash), ", size:", block_size
            )

            block_data = stream.read(block_size)

            if block_hash == b"\x00" * 32 and block_size == 0:
                break

            actual_block_hash = sha256(block_data)
            if actual_block_hash != block_hash:
                raise Exception(
                    f"Invalid block hash precompress. {_to_hex(actual_block_hash)} != {_to_hex(block_hash)}"
                )

            if self.dyn_header[HeaderField.COMPRESSION_FLAGS] == 1:
                print("\tcompressed block!")
                block_data = gzip.decompress(block_data)

            blocks[block_id] = block_data

        data = b""
        for block_id in blocks.keys():
            block = blocks[block_id]
            data += block

        self._decrypted_data = data
        self._root = BeautifulSoup(data, "xml")

    def get_meta(self):
        meta = self._root.Meta

        return {
            "meta": {
                "generator": meta.Generator.string,
                "dbname": meta.DatabaseName.string,
            },
        }

    def _fill_entries(self, group, gdata):
        for entry in group:
            if entry.name != "Entry":
                continue

            entry_id = _uuid(entry.UUID.string)
            gdata["entries"][entry_id] = {}
            edata = gdata["entries"][entry_id]

            for string in entry:
                if string.name != "String":
                    continue

                value = string.Value.string

                # check if there is a plaintext for the value
                actual_value = self._cipher_mappings.get(value)
                if actual_value:
                    value = actual_value

                edata[string.Key.string] = value

    def _start_salsa(self):
        stream_key = self.dyn_header[HeaderField.PROTECTEDSTREAMKEY]
        stream_key = sha256(stream_key)

        # the stream decryptor IV is hardcoded.
        stream_iv = [0xE8, 0x30, 0x09, 0x4B, 0x97, 0x20, 0x5D, 0x2A]
        stream_iv = bytes(stream_iv)

        self._stream = (stream_iv, stream_key)
        self._cur = 0

    def _salsa(self, length: int) -> str:
        (stream_iv, stream_key) = self._stream

        # print("salsa20 iv =", _to_hex(stream_iv))
        # print("salsa20 key =", _to_hex(stream_key))
        print("salsa20 counter", self._cur)
        print("salsa20 want to emit", length, "bytes")
        stream = Salsa20_keystream(self._cur + length, stream_iv, stream_key)
        # print("complete stream", _to_hex(stream))
        result: str = stream[self._cur : self._cur + length]
        print("actually wanted at end:", _to_hex(result))
        self._cur += length
        return result

    def _decrypt_stream(self, ciphertext: str) -> bytes:
        stream_id = self.dyn_header[HeaderField.INNERRANDOMSTREAMID]

        # setup ARC4 cipher
        """
        cipher = Cipher(
            algorithms.ARC4(stream_key),
            # ARC4 does not have IVs
            mode=None,
            backend=default_backend()
        )
        decryptor = cipher.decryptor()
        """

        def _do_salsa(x):
            stream = self._salsa(len(x))
            # print("ciphertext = ", _to_hex(x))
            # print("stream data = ", _to_hex(stream))
            res = _xor(x, stream)
            # print("result (hex) = ", _to_hex(res))
            return res

        def _do_arc4(x):
            raise Exception("ARC4 not supported.")

        stream_cipher = {0: lambda x: x, 1: _do_arc4, 2: _do_salsa,}[stream_id]

        ciphertext = base64.b64decode(ciphertext.encode())
        return stream_cipher(ciphertext)

    def _fill_mappings(self):
        if self._cipher_mappings:
            return

        self._start_salsa()

        # process over every Value with protected=true
        parser = etree.XMLParser()
        tree = etree.fromstring(self._decrypted_data, parser)
        for value_protected in tree.xpath("//Value[@Protected='True']/text()"):
            plain_password = self._decrypt_stream(value_protected)

            # we map ciphertexts to plaintexts instead of modifying the
            # ciphertext in the XML.
            self._cipher_mappings[value_protected] = plain_password

    def get_groups(self):
        res = {}
        self._fill_mappings()

        for group in self._root.Root:
            if group.name != "Group":
                continue

            group_id = _uuid(group.UUID.string)
            res[group_id] = {"name": group.Name.string, "entries": {}}

            # get children
            gdata = res[group_id]
            self._fill_entries(group, gdata)

        return res

    def close(self):
        """Release all resources related to the database."""
        if self.closed:
            return

        click.secho("Database closing", fg="green")

        self.closed = True
        self._fd.close()

        # info about db
        del self.dyn_header
        del self._payload_start

        # decrypted data on db
        del self._decrypted_data
        del self._root
        del self._cipher_mappings
