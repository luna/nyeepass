#!/usr/bin/env python3
import click
import getpass
import readline
import base64
import codecs
from pathlib import Path

from ruamel.yaml import YAML
from bs4 import BeautifulSoup

from .database import Database, DatabaseError
from .crypto import sha256
from .nyeesh import Nyeesh, err

DEFAULT_CONF_PATH = Path.home() / ".nyeepass.yml"
DEFAULT_NYEE_CONFIG = {
    "databases": {
        "name_for_database": {
            "path": "/somewhere/in/the/filesystem",
            "credentials": "keyfile",
            "keyfile": "~/key.kdbx",
        },
        "database_with_keyfile": {
            "path": "~/.somewhere/db.kdbx",
            "credentials": "password keyfile",
            "keyfile": "~/key.kdbx",
        },
        "database_just_password": {
            "path": "~/.somewhere/db.kdbx",
            "credentials": "password",
        },
    },
    "default_db": "name_for_database",
}


@click.group()
@click.pass_context
def cli(ctx):
    nyee_conf = YAML(typ="safe")

    try:
        cfg = nyee_conf.load(DEFAULT_CONF_PATH)
    except FileNotFoundError:
        nyee_conf.default_flow_style = False
        nyee_conf.dump(DEFAULT_NYEE_CONFIG, DEFAULT_CONF_PATH)
        print(f"Config file written to {DEFAULT_CONF_PATH}")
        cfg = DEFAULT_NYEE_CONFIG

    ctx.cfg = cfg
    default = cfg.get("default_db")
    if default:
        dbspec = cfg["databases"][default]
        ctx.default_db = Database(dbspec)


def read_keyfile(keyfile: Path) -> bytes:
    raw = keyfile.read_bytes()
    xml_start = b"<?xml"

    if raw[: len(xml_start)] == xml_start:
        xml = BeautifulSoup(raw, "xml")
        key = xml.Key.Data.string
        return base64.b64decode(key)

    if len(raw) == 32:
        return raw
    elif len(raw) == 64:
        return codecs.decode(raw, "hex")

    return sha256(raw)


@cli.command()
@click.argument("database_name")
@click.pass_context
def open(ctx, database_name):
    """Read a database defined in the configuration file.

    Spawns a console to copy passwords.
    """
    cfg = ctx.parent.cfg
    databases = cfg["databases"]

    try:
        dbspec = databases[database_name]
    except KeyError:
        err("Database not found")
        return

    if database_name == cfg.get("default_db"):
        database = ctx.parent.default_db
    else:
        database = Database(dbspec)

    database.fill()

    # the components for key construction
    components = []
    credentials = dbspec.get("credentials", "password").split()

    if "password" not in credentials:
        click.secho(
            f"No password credential for {database_name}, " "going with empty",
            fg="yellow",
        )

        # do blank password
        components.append(sha256(""))

    # process each credential
    for idx, credential in enumerate(credentials):
        if credential == "password":
            password = getpass.getpass(
                f"Credential {idx} (password) for {database_name}: "
            )

            components.append(sha256(password))
        elif credential == "keyfile":
            try:
                # get the declared keyfile,
                # read_keyfile will take care of decoding xml
                keyfile_path = dbspec["keyfile"]
                keyfile = Path(keyfile_path).expanduser()

                keydata = read_keyfile(keyfile)
                print(f"Credential {idx} (keyfile) for {database_name}")
                components.append(keydata)
            except KeyError:
                return err(f"No keyfile declared on database config")

    try:
        database.decrypt(components)
    except DatabaseError as err:
        err(f"Database error: {err!r}")
        return

    click.secho("Decryption success!", fg="green")

    data = database.get_meta()
    click.secho(f'DB Generator: {data["meta"]["generator"]}', fg="yellow")
    click.secho(f'DB Name: {data["meta"]["dbname"]}', fg="yellow")

    # initialize group info
    database.get_groups()

    nyeesh = Nyeesh(ctx=ctx, database=database)
    nyeesh.cmdloop(
        f"Welcome to nyeesh @ database {database_name}!\n"
        f"Use exit/quit/C-d to close."
    )
    database.close()
