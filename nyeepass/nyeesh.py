"""
nyeesh.py - nyeepass shell
"""
import sys
import time
import threading
from cmd import Cmd

import click
import pyperclip


def err(message):
    click.secho(message, fg="red")


class Nyeesh(Cmd):
    prompt = "nyeesh> "

    def __init__(self, *args, **kwargs):
        self.ctx = kwargs.pop("ctx")
        self.database = kwargs.pop("database")
        self._self_close = None
        self.to_close = False

        super().__init__(*args, **kwargs)
        self.groups = self.database.get_groups()

    def _close(self):
        click.secho("Database auto-closed after inactivity.", fg="yellow")
        self.database.close()

    def preloop(self):
        self._self_close = threading.Timer(900, self._close)
        self._self_close.start()

    def precmd(self, line):
        if self.database.closed:
            click.secho("Database is closed. Will not continue.", fg="yellow")
            return "exit"

        if self._self_close:
            self._self_close.cancel()

        # restart the selfdestruct thread
        self.preloop()

        return line

    def postcmd(self, stop, line):
        if self.to_close:
            self._self_close.cancel()

        return stop

    def do_test(self, line):
        print("hewwo >w<")

    def do_list(self, line):
        """List all groups in the database."""
        for group_id, group in self.groups.items():
            click.secho(f'Group "{group["name"]}"', fg="yellow")

            for entry_id, entry in group["entries"].items():
                estr = f'Title: {entry["Title"]}'
                click.secho(f" - {estr}", fg="blue")

    def get_entry(self, name):
        for group in self.groups.values():
            for entry in group["entries"].values():
                if entry["Title"] == name:
                    return entry

        return

    def do_search(self, search_terms):
        """search <terms>

        Search entries on the database
        """
        names = []
        groups = self.groups.values()

        for group in groups:
            entries = group["entries"].values()
            for entry in entries:
                title = entry["Title"].lower()
                if search_terms.lower() in title:
                    names.append(title)

        if not names:
            return err("No entries found")

        for name in names:
            click.secho(f" - {name}", fg="blue")

    def do_copypwd(self, entry_name):
        """copypwd <entry_name>

        Copy a password.
        """
        if not entry_name:
            return err("No argument provided: entry_name")

        entry = self.get_entry(entry_name)
        if not entry:
            return err(f"Entry {entry_name!r} not found")

        click.secho(f'Username: {entry["UserName"]}', fg="green")
        # click.secho(f'Password: {entry["Password"]}', fg='green')

        pyperclip.copy(entry["Password"].decode())
        click.secho(f"Password copied! 20 seconds until clipboard clear", fg="yellow")

        try:
            time.sleep(20)
        except KeyboardInterrupt:
            pass

        pyperclip.copy("")
        click.secho(f"Clipboard clear", fg="green")

    def complete_copypwd(self, text, line, begidx, endidx):
        names = []
        groups = self.groups.values()

        for group in groups:
            entries = group["entries"].values()
            for entry in entries:
                title = entry["Title"].lower()
                if title.startswith(text.lower()):
                    names.append(title)

        return names

    def _exit(self):
        self.to_close = True
        return True

    def do_EOF(self, line):
        return self._exit()

    def do_quit(self, line):
        return self._exit()

    def do_exit(self, line):
        return self._exit()
