from setuptools import setup

setup(
    name="nyeepass",
    version="0.1",
    py_modules=["nyeepass"],
    install_requires=[
        "Click==6.7",
        "ruamel.yaml==0.16.10",
        "cryptography==2.9.2",
        "pycryptodome==3.9.7",
        "beautifulsoup4==4.9.1",
        "lxml==4.5.1",
        "salsa20==0.3.0",
        "pyperclip==1.8.0",
        "Cython==0.29.19",
    ],
    entry_points="""
        [console_scripts]
        nyeepass=nyeepass:cli
    """,
)
